
let Repo = require('cry-db').Repo;

(async function () {
    
    let repo = new Repo("regs", "lacolonia", "mongodb://192.168.88.28")

    let regs = await repo.find({ year:2020})
    for await (let reg of regs) {
        if (reg.country && reg.country!="Swiss") continue;
        let c = calcCountry(reg.country || reg.nationality ||  reg.lang || reg.lang)
        if (c) await repo.updateOne({ _id: reg._id }, { country: c })
        else console.log("failed to match", reg.nationality || reg.lang, reg)
    }

    process.exit(0)
})();



function calcCountry(nationality)
{

    if (nationality.match(/austral/gi)) return "Australia";
    if (nationality.match(/austri/gi)) return "Austria";
    if (nationality.match(/bosn/gi)) return "BiH";
    if (nationality.match(/bih/gi)) return "BiH";
    if (nationality.match(/osterm/gi)) return "Switzerland";
    if (nationality.match(/hell/gi)) return "Greece";
    if (nationality.match(/bri/gi)) return "UK";
    if (nationality.match(/cro/gi)) return "Croatia";
    if (nationality.match(/deu/gi)) return "Germany";
    if (nationality.match(/esto/gi)) return "Estonia";
    if (nationality.match(/germ/gi)) return "Germany";
    if (nationality.match(/hrv/gi)) return "Croatia";
    if (nationality.match(/hun/gi)) return "Hungary";
    if (nationality.match(/isra/gi)) return "Israel";
    if (nationality.match(/ita/gi)) return "Italy";
    if (nationality.match(/nor/gi)) return "Norway";
    if (nationality.match(/pol/gi)) return "Poland";
    if (nationality.match(/russ/gi)) return "Russia";
    if (nationality.match(/slo/gi)) return "Slovenija";
    if (nationality.match(/si/gi)) return "Slovenija";
    if (nationality.match(/swe/gi)) return "Sweden";
    if (nationality.match(/swi/gi)) return "Switzerland";
    if (nationality.match(/dami|orlando|urbani/gi)) return "Italy";
    if (nationality.match(/en/gi)) return "Italy";
}