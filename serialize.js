const BSON = require('bson');
const { inflate, deflate } = require('pako');

BSON.setInternalBufferSize(500000000);

const serialize = BSON.serialize
const deserialize = BSON.deserialize

function pack(data) {
    if (data === null) data = { __null__: true }
    if (data === undefined) data = { __undef__: true }
    if (data === '') data = { __empty__: true }
    return Buffer.from(deflate(serialize({ l: data })));
}

function unpack(data) {
    if (!data) return data;
    try {
        if ((Buffer.from('null')).equals(data)) return null;  // HACK: this should not be necessary
        let ret = deserialize(inflate(new Uint8Array(data))).l;
        if (ret.__null__) return null;
        if (ret.__undef__) return undefined;
        if (ret.__empty__) return '';
        return ret;
    } catch (err) {
        console.error('unpack error', err, (data || '').toString());
        return data;
    }
}

module.exports =
{
    pack,
    unpack
}
