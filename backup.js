#!/usr/bin/env node

const Db = require("cry-db").Db;
const fs = require('fs');
const { pack } = require('./serialize');
const { program } = require('commander');
const dayjs = require('dayjs');

program.version('0.0.2');

program
    .description('creates a mongodb database backup file\nuse restore.js to restore a backup')
    .option('-s, --server <url>', 'mongo server')
    .option('-d, --debug', 'output extra debugging')
    .option('-S, --silent', 'do not output progress')
    .arguments('<db>')
    .action(async db => {
        program.server = program.server ? (program.server.match(/^mongodb:\/\//i) ? program.server : `mongodb://${program.server}`) : null;
        if (!program.silent) console.log('backup db', db, program.server ? `from server ${program.server}`:'');
        if (program.debug) console.log(program.opts())

        let mdb = new Db(db,program.server)
        let backup = await mdb.backupDb()

        let now = new Date()
        let filename = `${db}-${dayjs().format('YYYY-MM-DD-HH-mm')}.backup`
        if (!program.silent) console.log('backed-up', Object.keys(backup).reduce((p, k) => { p[k] = backup[k].length; return p; },{}));
        if (!program.silent) console.log('output file', filename);
        
        fs.writeFileSync(filename, pack(backup));

        process.exit(0)
    })

program.parse(process.argv);
