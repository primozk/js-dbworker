//@ts-check
const Db = require("cry-db");
const { program } = require('commander');
const helpers = require('cry-helpers');
const fs = require("fs")

program.version('0.0.1');

function stringify(s) {
    if (!s) return ""
    if (typeof s === "object") return ""
    if (typeof s === "number") return `${Intl.NumberFormat("sl-SI", { style: "decimal", useGrouping: false, minimumFractionDigits:2, maximumFractionDigits:2}).format(s)}`
    return '"'+s.toString()+'"'
}

function recordsetToCsv(ret) {
    let lines = []
    if (ret.length) {
        // headers
        let keys = Object.keys(ret[0]);
        let dataFields = Object.keys(ret[0]).filter(k => k != "_id");
        lines.push(keys.join(";"))
        // all lines
        for (let line of ret) {
            let l = []
            for (let key of keys) l.push(stringify(line[key]))
            lines.push(l.join(";"))
        }
    }
    return lines
}

program
    .description('executes a mongo find and returns results to console')
    .option('-s, --server <url>', 'mongo server')
    .option('-d, --debug', 'output extra debugging')
    .option('-D, --noid', 'do not export _ids')
    .option('-f, --field <field>', 'export this field of query')
    .option('-i, --include <include>', 'comma-separated list of fields to include')
    .option('-e, --exclude <exclude>', 'comma-separated list of fields to exclude')
    .option('-l, --limit <limit>', 'limit number of records returned')
    .option('-p, --project <project>', 'projection obj')
    .option('-o, --sort <sort>', 'sort obj')
    .option('-n, --dry-run', 'do not restore, just report')

    .arguments('<db> <collection> <pipelineFile>')
    .action(async (...params) => {
        let [db, collection, pipelineFile] = params

        program.server = program.server ? (program.server.match(/^mongodb:\/\//i) ? program.server : `mongodb://${program.server}`) : null;

        let pipelineObj = require(pipelineFile)

        let repo = new Db.Repo(collection, db, program.server); 
        let opts = {}
        let res = await repo.aggregate(pipelineObj, opts);
        
        if (program.debug) console.log('lines aggregated', res.length);
        if (program.debug) console.log('-------');

        let csv = recordsetToCsv(res)

        if (!program.dryRun)
            for (let line of csv)
                console.log(line);
        
        if (program.debug) {
            console.log(program.opts())
            console.log('db', db);
            console.log('collection', collection);
            console.log('query', pipelineObj)
            console.log('only field', program.field)
            if (res.length) {
                console.log('result fields', Object.keys(res[0]).join(","))
            }
        }

        process.exit(0)
    });

program.parse(process.argv);
