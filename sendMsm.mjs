
import { SmsSenderLogic } from "cry-sms-sender-logic";

var lastSmsSent = 0;
var msBetweenSmses = 30 * 1000;
var lastSmsTexts = [];
var reports = 0;

export function sendSms(text) {
    try {
        if (!process.env.SEND_SMS_ON_ERROR_TO) return;

        reports++;
        let tooEarly = (new Date().valueOf() - lastSmsSent) < msBetweenSmses;
        let newText = !lastSmsTexts.includes(text)
        lastSmsSent = new Date().valueOf();
        if (!lastSmsTexts.includes(text)) lastSmsTexts.push(text);

        if (tooEarly && !newText) return;

        text =
            (reports > 1 ? `${reports}x ` : '') +
            text +
            " at " + (new Date().toISOString());

        let smsLogic = new SmsSenderLogic({ sender: "klik.vet", cert: "jakom" });
        smsLogic.sendSmses([{
            gsm: parseInt(process.env.SEND_SMS_ON_ERROR_TO),
            text
        }]);
        reports = 0;

    } catch (err) {
        console.error("error sending SMS", err)
    }
}
