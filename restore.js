const Db = require("cry-db").Db;
const { unpack } = require('./serialize');
const fs = require('fs');
const { program } = require('commander');
const dayjs = require('dayjs');

program.version('0.0.1');

program
    .description('restores a mongodb database from backup file\nuse backuo.js to create a backup')
    .option('-s, --server <url>', 'mongo server')
    .option('-d, --debug', 'output extra debugging')
    .option('-S, --silent', 'do not output progress')
    .option('-n, --dry-run', 'do not restore, just report')
    .arguments('<db> <file>')
    .action(async (db,file) => {
        program.server = program.server ? (program.server.match(/^mongodb:\/\//i) ? program.server : `mongodb://${program.server}`) : null;
        if (!program.silent) console.log('restore to db', db, program.server ? `on server ${program.server}` : '');
        if (program.debug) console.log('input file', file);
        if (program.debug) console.log(program.opts())
        let mdb = new Db(db, program.server)
        let data = unpack(fs.readFileSync(file));
        if (program.dryRun) {
            console.log("would restore", Object.keys(data).reduce((p, k) => { p[k] = data[k].length; return p;},{}))
        } else {
            let res = await mdb.restoreDb(data)
            if (!program.silent) console.log('restored',res);
         }

        process.exit(0)
    })

program.parse(process.argv);
