//@ts-check

const chai = require('chai')
const should = require('chai').should();
const expect = require('chai').expect;
const { Broker, Client } = require('cry-ebus2');

let defaults = {
    // port: 5559
}
// // run ebus broker, client, and worker
let broker = new Broker(defaults)
broker.start()
const { DbWorker } = require('../dbworker');
let worker = new DbWorker(defaults)
worker.start()
let client = new Client(defaults)

async function sleep(ms) {
    return new Promise((resolve) => {
    setTimeout(resolve, ms);
    });
};

describe('dbworker', async function () {

    process.env.MONGO_DB = "test"
    process.env.AUDIT_COLLECTIONS = "test.test"

    it('should have a broker', async function () {
        broker.should.not.equal(null)
    });
    it('should have a client', async function () {
        client.should.not.equal(null)
    });
    
    it('worker should respond to a ping', async function () {
        let res = await client.request('db', { operation: 'ping' })
        expect(res.result).to.equal("pong")
    });
    it('should return collections', async function () {
        let a = await client.request('db', { operation: 'getCollections', db: 'test' });
        expect(a).to.be.exist;
        expect(a instanceof Array).to.be.true
    });


    it('should delete everything to start off', async function () {
        let del = await client.request('db', { operation: 'hardDelete', query: {}, collection: 'c' })
    });
    it('should find inner property', async function () {
        let a = await client.request('db', { operation: 'insert', insert: { a: { b: 1}, c:2 }, collection: 'c' })
        let b = await client.request('db', { operation: 'findOne', query: { "a.b": 1 }, collection: 'c' })
        expect(b.c).to.eq(2)
        expect(b.a.b).to.eq(1)
    });

    it('should not fail on non-existing inner property', async function () {
        let b = await client.request('db', { operation: 'findOne', query: { "a.b.c.d": 1 }, collection: 'c' })
        expect(b).to.not.exist
    });

    it('should test hash', async function () {
        try {
            let a = await client.request('db', { operation: 'upsert',   db: 'test', collection: 'test', query: { a: 123321 }, update: {  __hashed__p: "abc" } }, { receiveTimeout:2000});
            let t = await client.request('db', { operation: 'testHash', db: 'test', collection: 'test', query: { a: 123321 }, field: "p", value: "abc" }, { receiveTimeout:2000});
            let f = await client.request('db', { operation: 'testHash', db: 'test', collection: 'test', query: { a: 123321 }, field: "p", value: "cba" }, { receiveTimeout:2000});
            expect(t).to.be.true;
            expect(f).to.be.null
        } catch (err) {
            console.error("test hash", err)
            
        }
    });

    it('worker should upsert a nested object', async function () {
        let res = await client.request('db', { operation: 'upsert', query: { xq: 1,}, update: { a: { b: 3}} , collection:'c' })
        should.exist(res._id)
        let r2 = await client.request('db', { operation: 'findById', id: res._id, collection: 'c' })
        r2.a.b.should.equal(3)
    });  
    it('worker should update a nested object', async function () {
        let res = await client.request('db', { operation: 'updateOne', query: { xq: 1, }, update: { a: { b: 5 } }, collection: 'c' })
        should.exist(res._id)
        let r2 = await client.request('db', { operation: 'findById', id: res._id, collection: 'c' })
        r2.a.b.should.equal(5)
    }); 
    it('worker should insert', async function () {
        let res = await client.request('db', { operation: 'insert', insert: { x: 1 }, collection: 'c' })
        should.exist(res._id)
    });  
    it('worker should upsertBatch, skipping over one error', async function () {
        let res =  await client.request('db',{ operation: 'upsertBatch', collection:"c",  batch:[ 
            { query: { ba:1 }, update: {  ab: 1 } },
            { query: { ba:2 }, update: {  ab: 2 } },
            { query: {      }, update: null },
            { query: { ba:3 }, update: {  ab: 3 } },
        ]});
        let q = JSON.stringify({ operation: 'find', query:{ ba: { $exists: true } }, collection:"c", opts: { sort: { ba: 1  }} });
        let f = await client.request('db', JSON.parse(q) )
        f.length.should.equal(3)
        f[2].ab. should.equal(3)
    });  
    
    it('worker should insert then findOne with JSON', async function () {
        let res =  await client.request('db',{ operation: 'insert',  insert: {x:222} , collection:'c' })
        let q = JSON.stringify({ operation: 'findOne', query:{ _id: res._id }, collection:"c" });
        let f = await client.request('db', JSON.parse(q) )
        should.exist(res._id)
        f._id.toString().should.equal(res._id.toString())
    });      
    
    it('worker should insert many', async function () {
        let res =  await client.request('db',{ operation: 'insertMany',  insert: [{x:11},{x:12}] , collection:'c' })
        res.length.should.equal(2)
        res[0].x.should.equal(11)
        res[1].x.should.equal(12)
    });  
    it('worker delete all', async function () {
        let res =  await client.request('db',{ operation: 'delete',  query: {} , collection:'c' })
    });  
    it('findAll should project,sort,skip, and limit', async function () {
        await client.request('db',{ operation:'upsert', query:{ a: 75}, update: { b:1, p:2 }, collection:'c' });
        await client.request('db',{ operation:'upsert', query:{ a: 73}, update: { b:2, p:2 }, collection:'c' });
        await client.request('db',{ operation:'upsert', query:{ a: 71}, update: { b:3, p:2 }, collection:'c' });
        await client.request('db',{ operation:'upsert', query:{ a: 76}, update: { b:4, p:2 }, collection:'c' });
        await client.request('db',{ operation:'upsert', query:{ a: 77}, update: { b:4, p:2 }, collection:'c' });
        await client.request('db',{ operation:'upsert', query:{ a: 72}, update: { b:4, p:2 }, collection:'c' });
        await client.request('db',{ operation:'upsert', query:{ a: 74}, update: { b:4, p:2 }, collection:'c' });
        
        let res = await client.request('db',{ operation:'findAll', query:{ p: 2 }, collection:'c', opts: {
            project: { b:false },
            sort: { a: 1 },
            skip: 2,
            limit: 3
        }});

        res = res;
        
        
        expect(res.length).to.equal(3)
        expect(res[0].a).to.equals(73)
        expect(res[0].b).to.not.exist
    });  
    it('find should project,sort,skip, and limit', async function () {
        await client.request('db',{ operation:'upsert', query:{ a: 75}, update: { b:1, p:3 }, collection:'c' });
        await client.request('db',{ operation:'upsert', query:{ a: 73}, update: { b:2, p:3 }, collection:'c' });
        await client.request('db',{ operation:'upsert', query:{ a: 71}, update: { b:3, p:3 }, collection:'c' });
        await client.request('db',{ operation:'upsert', query:{ a: 76}, update: { b:4, p:3 }, collection:'c' });
        await client.request('db',{ operation:'upsert', query:{ a: 77}, update: { b:4, p:3 }, collection:'c' });
        await client.request('db',{ operation:'upsert', query:{ a: 72}, update: { b:4, p:3 }, collection:'c' });
        await client.request('db',{ operation:'upsert', query:{ a: 74}, update: { b:4, p:3 }, collection:'c' });
        
        let res = await client.request('db',{ operation:'find', query:{ p: 3 }, collection:'c', opts: {
            project: { b:false },
            sort: { a: 1 },
            skip: 2,
            limit: 3
        }});

        res = res;
        
        
        expect(res.length).to.equal(3)
        expect(res[0].a).to.equals(73)
        expect(res[0].b).to.not.exist
    });     
    it('findOne should project', async function () {
        await client.request('db',{ operation:'insertMany', insert:[{s:1},{s:2,q:3}], collection:'c' });
        
        let r1 = await client.request('db',{ operation:'findOne', query:{ s:2 }, collection:'c', projection:{s:false}});
        expect(r1.q).to.exist
        expect(r1.s).to.not.exist
    });              
    it('should soft, skip, and limit on find', async function () {
        await client.request('db',{ operation:'insertMany', insert:[ {v:2, b:6},{v:2, b:3}, {v:2, b:2}, {v:2, b:4},{v:2, b:5},{v:2, b:1},{v:2, b:7} ], collection:'c' });
        let res = await client.request('db',{ operation:'find', query:{ v: 2 }, collection:'c', opts: {
            project: { v: false, _id:false },
            sort: { b: 1 },
            skip: 2,
            limit: 3
        }});        
        let ret = res;
        ret.length.should.equal(3);
        expect(ret[0].v).to.not.exist;
        ret[0].b.should.equal(3)
        ret[1].b.should.equal(4)
        ret[2].b.should.equal(5)
    });    
    it('shoud insert a sequence', async function () {
        await client.request('db',{ db:'test', collection:'c', operation:'hardDelete', query:{} })
        await client.request('db',{ db:'test', collection:'c', operation: 'insert', insert:{ k: 1, s:'SEQ_LAST' }})
        await client.request('db',{ db:'test', collection:'c', operation: 'insert', insert:{ k: 2, s:'SEQ_NEXT' }})
        await client.request('db',{ db:'test', collection:'c', operation: 'insert', insert:{ k: 3, s:'SEQ_LAST' }})
        await client.request('db',{ db:'test', collection:'c', operation: 'insert', insert:{ k: 4, s:'SEQ_NEXT' }})
       
        let r1 = await client.request('db',{ db:'test', collection:'c', operation:'find', query:{k:1}});
        let r2 = await client.request('db',{ db:'test', collection:'c', operation:'find', query:{k:2}});
        let r3 = await client.request('db',{ db:'test', collection:'c', operation:'find', query:{k:3}});
        let r4 = await client.request('db',{ db:'test', collection:'c', operation:'find', query:{k:4}});

        expect(r1[0].s).to.equal(0)
        expect(r2[0].s).to.equal(1)
        expect(r3[0].s).to.equal(1)
        expect(r4[0].s).to.equal(2)
    });     
    it('worker should insert then exist', async function () {
        let res =  await client.request('db',{ operation: 'insert', insert:{ x:1} , collection:'c' })
        should.exist(res._id)
        let res2 = await client.request('db',{ operation: 'findById',  id:res._id , collection:'c' })
        res._id.should.equal(res._id)
    });  
 
    it('worker should upsert then exist', async function () {
        let a1 = await client.request('db',{ operation: 'upsert', query: {x:2}, update: { a:1 }  , collection:'c' })
        let a2 = await client.request('db',{ operation: 'upsert', query: {x:2}, update: { a:2, b:2 } , collection:'c' })
        let res = await client.request('db',{ operation: 'findOne', query: {x:2}, collection:'c' })
        res.x.should.equal(2)
        res.a.should.equal(2)
        res.b.should.equal(2)
    });      
    it('worker should update one', async function () {
        await client.request('db',{ operation: 'upsert', query: {x:3}, update: { a:1,b:0 }  , collection:'c' })
        await client.request('db',{ operation: 'updateOne', query: {x:3}, update: { a:2, b:2 }  , collection:'c' })
        let res = await client.request('db',{ operation: 'findOne', query: {x:2}, collection:'c' })
        res.x.should.equal(2)
        res.a.should.equal(2)
        res.b.should.equal(2)
    });  
    it('worker should update and find many', async function () {
        let r1 = await client.request('db',{ operation: 'insert', insert: { y:0, a:1, b:1  }  , collection:'c' })
        let r2 = await client.request('db',{ operation: 'insert', insert: { y:0, a:2, b:2  }  , collection:'c' })
        let up = await client.request('db',{ operation: 'update', query: {y:0}, update:{a:3}, collection:'c' })
        let rs = await client.request('db',{ operation: 'find', query: {y:0}, collection:'c' })
        let rr1 = rs.find( a => a._id.toString()==r1._id.toString())
        let rr2 = rs.find( a => a._id.toString()==r2._id.toString())
        should.exist(rr1)
        should.exist(rr2)
        rr1.a.should.equal(3)
        rr2.a.should.equal(3)
        rr1.b.should.equal(1)
        rr2.b.should.equal(2)
    });  
    
    it('worker should delete many', async function () {
        let r1 = await client.request('db',{ operation: 'insert', insert: { y:1, a:1, b:1  }  , collection:'c' })
        let r2 = await client.request('db',{ operation: 'insert', insert: { y:1, a:2, b:2  }  , collection:'c' })
        let r3 = await client.request('db',{ operation: 'delete', query: { y:1,  }  , collection:'c' })
        let r4 = await client.request('db',{ operation: 'findOne', query: { y:1,  }  , collection:'c' })
        should.not.exist(r4)
    });  
    it('worker should delete one', async function () {
        let r1 = await client.request('db',{ operation: 'insert', insert: { y:2, a:1, b:1  }  , collection:'c' })
        let r2 = await client.request('db',{ operation: 'insert', insert: { y:2, a:2, b:2  }  , collection:'c' })
        let r3 = await client.request('db',{ operation: 'deleteOne', query: { y:2, a:1 }  , collection:'c' })
        let r4 = await client.request('db',{ operation: 'findOne', query: { y:2 }  , collection:'c' })
        let r5 = await client.request('db',{ operation: 'findOne', query: { y:1 }  , collection:'c' })
        should.exist(r4)
        should.not.exist(r5)
        r4.a.should.equal(2)
        r4.b.should.equal(2)
    });  
    it('worker should soft delete one', async function () {
        let r1 = await client.request('db',{ operation: 'insert', insert: { y:24432 }  , collection:'c' })
        let r2 = await client.request('db',{ operation: 'findOne', query: { y:24432 }  , collection:'c' })
        should.exist(r2);
                 await client.request('db',{ operation: 'delete', query: { y:24432 }  , collection:'c' })
        let r3 = await client.request('db',{ operation: 'findOne', query: { y:24432 }  , collection:'c' })
        should.not.exist(r3);
        let r4 = await client.request('db',{ operation: 'findAll', query: { y:24432 }  , collection:'c' })
        r4.length.should.equal(1)
    });  
    it('worker should find newer', async function () {
        let r0 = await client.request('db',{ operation: 'hardDelete', query:{} , collection:'c' })
        let r1 = await client.request('db',{ operation: 'insert', insert: { y:5, a:0, b:1  }  , collection:'c' })
        let r2 = await client.request('db',{ operation: 'insert', insert: { y:6, a:0, b:2  }  , collection:'c' })
        let r3 = await client.request('db',{ operation: 'insert', insert: { y:7, a:0, b:3  }  , collection:'c' })
        let r4 = await client.request('db',{ operation: 'insert', insert: { y:8, a:0, b:4  }  , collection:'c' })
        let r5 = await client.request('db',{ operation: 'findOne', query: { y:6 }  , collection:'c' })
        r5._ts.should.exist;
        let r6 = await client.request('db',{ operation: 'findNewer', timestamp: r5._ts  , collection:'c' })
        r6.length.should.equal(2)
        r6[0].y.should.equal(7)
        r6[0].b.should.equal(3)
        r6[1].y.should.equal(8)
        r6[1].b.should.equal(4)
    });      
    it('worker should reject on no collection', async function () {
        client.request('db',{ operation: 'insert', data:{ x:1 }   })
        .catch(res => { res.reason.should.equal('reject')})
    });
    it('worker should reject on no data', async function () {
        client.request('db',)
        .catch(res => { res.reason.should.equal('reject')})

    });
    it('worker should reject on no operation', async function () {
        client.request('db',{ })
        .catch(res => { res.reason.should.equal('reject')})

    });
    it('worker should reject unknown operation', async function () {
        await client.request('db',{ operation: 'nosuchop', collection:'c' },{receiveTimeout:200})
        .catch(res => { res.reason.should.equal('reject')})

    });


    it('worker should soft delete one', async function () {
        await client.request('db',{ operation: 'insertMany', insert:[{r:2,p:1},{r:2,p:2}]  , collection:'c' })
        await client.request('db',{ operation: 'deleteOne', query:{r:2,p:1}, collection:'c'})
        let res1 = await client.request('db',{ operation: 'find', query: {r:2,p:1}, collection:'c' })
        let res2 = await client.request('db',{ operation: 'find', query: {r:2,p:2}, collection:'c' })
        res1.length.should.equal(0)
        res2.length.should.equal(1)
        let res3 = await client.request('db',{ operation: 'findAll', query: {r:2,p:1}, collection:'c' })
        res3.length.should.equal(1)
        res3[0]._deleted.should.exist
    });         

    it('worker should not find blocked', async function () {
        await client.request('db',{ operation: 'insertMany', insert:[{r:3,p:1},{r:3,p:2}]  , collection:'c' })
        await client.request('db',{ operation: 'blockOne', query:{r:3,p:1}, collection:'c'})
        let res1 = await client.request('db',{ operation: 'find', query: {r:3,p:1}, collection:'c' })
        let res2 = await client.request('db',{ operation: 'find', query: {r:3,p:2}, collection:'c' })
        res1.length.should.equal(1)
        res1[0]._blocked.should.exist
        res2.length.should.equal(1)
        let res3 = await client.request('db',{ operation: 'findAll', query: {r:3,p:1}, collection:'c' })
        res3.length.should.equal(1)
        res3[0]._blocked.should.exist
    });         

    it('worker should hard deleteOne', async function () {
        await client.request('db',{ operation: 'insertMany', insert:[{r:4,p:1},{r:4,p:2}]  , collection:'c' })
        await client.request('db',{ operation: 'deleteOne', query:{r:4,p:1}, collection:'c'})
        let res1 = await client.request('db',{ operation: 'find', query: {r:4,p:1}, collection:'c' })
        let res2 = await client.request('db',{ operation: 'find', query: {r:4,p:2}, collection:'c' })
        res1.length.should.equal(0)
        res2.length.should.equal(1)
        let res3 = await client.request('db',{ operation: 'findAll', query: {r:4,p:1}, collection:'c' })
        res3.length.should.equal(1)
        res3[0]._deleted.should.exist
        await client.request('db',{ operation: 'hardDeleteOne', query:{r:4,p:1}, collection:'c'})
        let res4 = await client.request('db',{ operation: 'findAll', query: {r:4,p:1}, collection:'c' })
        res4.length.should.equal(0)
    });         

    it('worker should hard delete many', async function () {
        await client.request('db',{ operation: 'insertMany', insert:[{r:5,p:1},{r:5,p:2}]  , collection:'c' })
        await client.request('db',{ operation: 'delete', query:{r:5}, collection:'c'})
        let res1 = await client.request('db',{ operation: 'find', query: {r:5,p:1}, collection:'c' })
        let res2 = await client.request('db',{ operation: 'find', query: {r:5,p:2}, collection:'c' })
        res1.length.should.equal(0)
        res2.length.should.equal(0)
        let res3 = await client.request('db',{ operation: 'findAll', query: {r:5}, collection:'c' })
        res3.length.should.equal(2)
        res3[0]._deleted.should.exist
        await client.request('db',{ operation: 'hardDelete', query:{r:5}, collection:'c'})
        let res4 = await client.request('db',{ operation: 'findAll', query: {r:5}, collection:'c' })
        res4.length.should.equal(0)
    });     
    
    it('worker should purge changes', async function () {

        await client.request('db',{ operation: 'purgeChangesForCollection', db:'test', collection:'test' })
        let log = await client.request('db',{ operation: 'getChangesForCollection', db:'test', collection:'test' })
        expect(log).to.be.empty;     
    });
    it('worker should log changes', async function () {

        let log
        await client.request('db',{ operation: 'purgeChangesForCollection', db:'test', collection:'test' })
        await client.request('db',{ operation: 'hardDelete', query:{}, db:'test', collection:'test' })
     
        log = await client.request('db',{ operation: 'getChangesForCollection', db:'test', collection:'test' })
        expect(log).to.be.empty;     

        await client.request('db',{ operation: 'insert', insert:{a:1}, db:'test', collection:'test' })
        await client.request('db',{ operation: 'insert', insert:{a:2}, db:'test', collection:'test' })
        await client.request('db',{ operation: 'updateOne', query:{a:1}, update:{b:1}, db:'test', collection:'test', _auth:{username:"racman"} })
        
        log = await client.request('db',{ operation: 'getChangesForCollection', db:'test', collection:'test' })
        expect(log.length).to.equal(3)

        let id =   await client.request('db',{ operation: 'findOne', query:{a:1}, db:'test', collection:'test' })
        log = await client.request('db',{ operation: 'getChangesForEntity', db:'test', collection:'test', _id:id._id })
    
        expect(log.length).to.equal(2)
        expect(log[0].user).to.equal("racman")

    });

  

    // it('should publish', async function () {
        
    //     let clientMessage 
    //     client.on('message',(m,c) => { console.log( clientMessage=m )})
    //     client.subscribe('db')

    //     await client.request('db',{ operation: 'insert', insert:{a:1}, db:'test', collection:'test' })
    //     await sleep(1000)

    //     console.log('clientMessage',clientMessage);
        
    // })

    it('message should expire if broker offline', async function () {
        broker.stop();
        let a = await client.request('db',{ operation: 'nosuchop', collection:'c' })
        .catch(res => { res.reason.should.equal('timeout')})
        should.not.exist(a)
    });

    it('message should expire if broker offline', async function () {
        broker.stop();
        let a = await client.request('db',{ operation: 'nosuchop', collection:'c' })
        .catch(res => { res.reason.should.equal('timeout')})
        should.not.exist(a)
    });



    it('should fail', async function () {
        broker.stop();
        let a = await client.request('db',{ operation: 'fail', collection:'c' })
        .catch(res => { expect(res.reason).to.eq('timeout')})
        should.not.exist(a)
    });

    setTimeout(() => {
        broker.stop();
        worker.stop();
        process.exit(0)
    },5000)
});