//@ts-check
const chai = require('chai')
const should = require('chai').should();
const expect = require('chai').expect;
const { Broker, Client } = require('cry-ebus2');

let defaults = {
    // port: 5559
}
// // run ebus broker, client, and worker
let broker = new Broker(defaults)
broker.start()
const { DbWorker } = require('../dbworker');
let worker = new DbWorker(defaults)
worker.start()

let client = new Client(defaults)

describe('dbworker', async function () {

    // let setup

    process.env.MONGO_DB = "test"
    process.env.AUDIT_COLLECTIONS = "test.test"

    var exec = require('child_process').exec;
    function stopMongo()
    {
        exec('brew services stop mongodb-community', function callback(error, stdout, stderr) {
        });
    }
    function startMongo() {
        var exec = require('child_process').exec;
        exec('brew services start mongodb-community', function callback(error, stdout, stderr) {
        });
    }

    it('should have a broker', async function () {
        exec('brew services stop mongodb-community', async function callback(error, stdout, stderr) {

            console.log('mongo stopped, running tsts');
            {   // run test
                let b = await client.request('db', { operation: 'findOne', query: { "a.b": 1 }, collection: 'c' },{ receiveTimeout:1000 })
                expect(b.c).to.eq(2)
                expect(b.a.b).to.eq(1)
            }

            exec('brew services start mongodb-community', async function callback(error, stdout, stderr) {
                console.log('mongo started');
                process.exit(0)
            });
        });
    });


    setTimeout(() => {
        broker.stop();
        worker.stop();
        process.exit(0)
    }, 30000)

})