//@ts-check
import dotenv from "dotenv"
dotenv.config()

import { DbWorker } from './dbworker.mjs';
new DbWorker().start();
