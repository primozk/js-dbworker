//@ts-check
const Db = require("cry-db");
const { program } = require('commander');
const helpers = require('cry-helpers');

program.version('0.0.1');

function stringify(s) {
    if (!s) return ""
    if (typeof s === "number") return s
    if (typeof s === "object") return ""
    return '"'+s.toString()+'"'
}

function recordsetToCsv(ret) {
    let lines = []
    if (ret.length) {
        // headers
        let keys = Object.keys(ret[0]).filter(k => !!k);
        lines.push(keys)
        // all lines
        for (let line of ret) {
            let l = []
            for (let key of keys) l.push(stringify(line[key]))
            lines.push(l.join(","))
        }
    }
    return lines
}

program
    .description('executes a mongo find and returns results to console')
    .option('-s, --server <url>', 'mongo server')
    .option('-d, --debug', 'output extra debugging')
    .option('-D, --noid', 'do not export _ids')
    .option('-f, --field <field>', 'export this field of query')
    .option('-i, --include <include>', 'comma-separated list of fields to include')
    .option('-e, --exclude <exclude>', 'comma-separated list of fields to exclude')
    .option('-l, --limit <limit>', 'limit number of records returned')
    .option('-p, --project <project>', 'projection obj')
    .option('-o, --sort <sort>', 'sort obj')
    .option('-n, --dry-run', 'do not restore, just report')

    .arguments('<db> <collection> [query]')
    .action(async (...params) => {
        let [db, collection, query] = params

        program.server = program.server ? (program.server.match(/^mongodb:\/\//i) ? program.server : `mongodb://${program.server}`) : null;

        let queryObj = JSON.parse(helpers.objStrToJson(query || "{}"))
        let flds1 = (program.include || "").split(",").map(f => f.trim()).filter(f => f)
        let flds0 = (program.exclude || "").split(",“").map(f => f.trim()).filter(f => f)

        let repo = new Db.Repo(collection, db, program.server); 
        let opts = {}
        if (program.project) opts.project = helpers.objStrToJson(program.project);
        if (program.sort) opts.project = helpers.objStrToJson(program.sort);
        let res = await repo.find(queryObj, opts);
        if (program.limit) res =res.slice(0,Number(program.limit))
        
        res = res.map(f => {
            
            let _id = f._id.toString()
            
            if (program.field) f = f[program.field]
            if (!f) return null;
            let out = { _id }
            if (flds1.length) flds1.forEach(k => out[k] = f[k])
            else out = { _id, ...f }
            if (flds0.length) flds0.forEach(k => delete out[k])
            if (program.noid) delete out._id;
            return out
        }).filter(f => f);

        if (program.debug) console.log('lines exported', res.length);
        if (program.debug) console.log('-------');

        let csv = recordsetToCsv(res)

        if (!program.dryRun)
            for (let line of csv)
                console.log(line);
        
        if (program.debug) {
            console.log(program.opts())
            console.log('db', db);
            console.log('collection', collection);
            console.log('query', queryObj)
            console.log('only field', program.field)
            if (res.length) {
                console.log('result fields', Object.keys(res[0]).join(","))
            }
        }

        process.exit(0)
    });

program.parse(process.argv);
