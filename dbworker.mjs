//@ts-check

const DEFAULT_SERVICE_NAME = 'db'
const DEFAULT_AUDIT_TO_COLLECTION = 'dblog'

import { Worker } from "cry-ebus2";
import { Repo as Repo } from "cry-db";
import { Mongo as Mongo } from "cry-db";
import { Base as Base } from "cry-db";
import { AssertionError } from "chai";
import { Log } from "cry-helpers";
const log = new Log(['DBWORKER','DB'])
import packageJson from "./package.json" assert { type: "json" };
import dayjs from "dayjs"
import { sendSms } from "./sendMsm.mjs";

function assert(c, msg) {
    if (!c) throw new AssertionError(msg)
}

if (process.env.DEBUG) sendSms(`db-worker ${process.pid} started`);


async function execInBatches(obj, batchSize, max, f, where = ((val) => true), preprocess = ((val) => { })) {
    let cnt = 0, totalcnt = 0;
    let batch = []
    for (let val of Object.values(obj)) {
        if (!where(val)) continue;
        preprocess(val);
        if (max && ++totalcnt > max) break;
        batch.push(val)
        if (++cnt === batchSize) {
            await f(batch)
            batch = []
            cnt = 0
        }
    }
    if (cnt) await f(batch)
}



async function sleep(msec, ret) {
    return new Promise(resolve => setTimeout(
        () => {
            resolve(ret)
        },
        msec)
    );
}

export class DbWorker extends Worker {

    constructor(options = {}) {
        super(DEFAULT_SERVICE_NAME, options)
        this.collectPublications = null
        this._publishMsg = options.publishMsg || true;
        this.auditTableName = process.env.AUDIT_TO_COLLECTION || DEFAULT_AUDIT_TO_COLLECTION
        this.audit = (process.env.AUDIT_COLLECTIONS || '').toString().split(',').map(a => a.trim().toLowerCase()).filter(a => !!a)
        this.forceError = !!process.env.DB_FORCE_ERROR
        console.log('starting worker for service ', this.service, 'pid', process.pid);
        if (this.audit.length) console.info('auditing collectinos ', this.audit, ' to  ', this.auditTableName)
    }

    async start()
    {
        await super.start()
    }

    async publishMsg(what) {

        if (this.collectPublications) {
            this.collectPublications.push(what)
        } else {
            await super.publish(what.channel, what.payload);
        }
    }

    async reset()
    {
        try {
            if (this.repo) await this.repo.close()
            delete this.repo;
        } catch (err) {
            console.log('db worker error on reset/close',err);
        }
        try {
            this.repo = new Repo(null);
            this.repo.emitPublishEvents(true);
            this.repo.on('publish', this.publishMsg.bind(this))
            this.repo.useAuditing(true);
        } catch (err) {
            console.log('db worker error on reset/establish', err);
        }
    }
    

    async onError(reason) {
        sendSms("db-worker error"+reason)
        console.error(`db/onError ${dayjs().format("YYYY-MM-DDTHH:mm:ss")}`);
        console.error(reason);
        await this.reset()
    }

    // worker functionality
    async process(data, opts) {
        let ret
        try {
            if (this.forceError) throw new Error(`DB_FORCE_ERROR ${data.collection} ${data.operation}`)
            await this.reset();
            ret = await this.processDb(data, opts)
        }
        catch (err) {
            console.error('');
            console.error(`dbworker/process error ${dayjs().format("YYYY-MM-DDTHH:mm:ss")}`)
            console.error(err.message)
            console.error(data);
            throw err
        }
        await this.repo.close()
        return ret
    }

    async processDb(data, opts) {

        if (data?.collection!=="syscommands" && data?.operation!=="find")
            console.log(data.db||"db?", opts?.audit?.user||"user?", opts?.audit?.naprava_naziv||"device?"," - ", data.operation, data.collection || '', " - ", data._id || data.id || data.query?._id || '', dayjs().format("YYYY-MM-DDTHH:mm:ss"));
        
        if (data==='ping') {
            return 'pong';
        }

        if (data?.operation==='ping') {
            return { result: 'pong' };
        }

        if (data === 'pingdb') {
            return (await (new Base()).ping());
        }
        
        if (data==='newid') {
            return  (await Repo.newid());
        }       
        
        this.repo?.useSync(true);
        this.repo?.useAuditing(true);
        this.repo?.useRevisions(true);
        this.repo?.useSoftDelete(true);
        
        assert(data, 'missing data object with collection, operation');
        assert(data.operation, 'missing data.operation');
        
        let user = opts?.user || opts?.audit?.user
        if (user) this.repo.setUser(user);
        if (opts?.audit) this.repo.setAudit(opts?.audit)
        
        if (data.operation==='ping') {
            return  { ping:'pong' }
        }

        if (data.operation === 'sleep') {
            console.log('sleep requested', data);
            await sleep(data.sleep)
            console.log('sleep finished');
            
            return data
        }

        if (data.operation === 'throw') {
            console.log('throw requested', data);
            throw new Error(data.error||"throw requsted by client")
        }

        if (data === 'ver') {
            return packageJson.version
        }
        
        if (data.operation==='newid') {
            return  { _id: await Repo.newid() };
        }        
        
        data.db = data.db || process.env.MONGO_DB || "local"
        if (data.db) this.repo.setDb(data.db);

        if (data.operation === 'getCollections') {
            let ret = await this.repo.getCollections();
            return ret;
        }

        if (data.operation === 'backupDb') {
            this.repo.useSoftDelete(false);
            let colls = await this.repo.getCollections();
            let backup = {}
            for await (let col of colls) {
                this.repo.setCollection(col);
                backup[col] = await this.repo.find({})
            }
            
            return backup;
        }

        if (data.operation === 'restoreDb') {
            assert(data.db, 'missing data.db');
            assert(data.collections, 'missing data.collections');

            this.repo.useSoftDelete(false);
            let colNames = Object.keys(data.collections);

            let restored={}
            for await (let col of colNames) {
                this.repo.setCollection(col);
                await this.repo.hardDelete({});
                let i = 0;
                await execInBatches(data.collections[col], 10000, null, async (batch) => {
                    await this.repo.insertMany(batch)
                    i += batch.length
                });
                if (i !== data.collections[col].length) throw new Error(`restored ${i} records instead of ${data.collections[col].length} for ${col}`);
                restored[col] = i;
            };
            return restored;
        }

        if (data.operation === 'findNewerMany') {
            assert(data.db, 'missing data.db');
            assert(data.collectionsSpec, 'missing data.collectionsSpec');

            async function loadOneNewer(db,collection, timestamp, query, opts) {
                try {
                    let mongo = new Mongo(db)
                    let data = await mongo.findNewer(collection, timestamp, query, opts);
                    mongo.close()
                    return {collection,data};
                } catch (error) {
                    console.error(`findNewerMany error for ${collection} ${query} ${opts} ${error.message || error}`)
                    return {collection,data}
                }
            }

            let promises = data.collectionsSpec.map(spec => loadOneNewer(data.db,spec.collection, spec.timestamp, spec.query || {}, spec.opts || data.opts || {}))
            return (await Promise.all(promises)).filter(r => r.data?.length || r.error);
        }
        
        assert(data.collection, 'missign data.collection');
        this.repo.setCollection(data.collection);
        
        if (data.operation==='findNewer') {
            assert(data.timestamp!==null, 'missing data.timestamp');
            let ret = await this.repo.findNewer(data.timestamp,data.query,data.opts)
            return ret;
        }

        if (data.operation==='findNewerFromDate') {
            assert(data.timestamp, 'missing data.data');
            let ret = await this.repo.findNewerFromDate(data.date,data.query,data.opts)
            return ret;
        }
        if (data.operation==='findById') {
            assert(data.id, 'missing data.id');
            let ret = await this.repo.findById(data.id, data.projection || data.opts && data.opts.project)
            return ret;
        }
        if (data.operation==='findOne') {
            assert(data.query, 'missing data.query');
            let ret = await this.repo.findOne(data.query, data.projection || data.opts && data.opts.project)
            return ret;
        }
        if (data.operation==='findAll') {
            assert(data.query, 'missing data.query');
            let ret = await this.repo.findAll(data.query, data.opts)
            return ret;
        }        
        if (data.operation==='insert') {
            assert(data.insert!=null, 'missing data.insert')
            let ret = await this.repo.insert(data.insert)
            if (!ret) return null
            return { ...ret }
        }
        if (data.operation==='insertMany') {
            assert(data.insert!=null, 'missing data.insert')
            let rets = await this.repo.insertMany(data.insert)
            return rets
        }
        if (data.operation==='upsert') {
            assert(data.query, 'missing data.query object')
            assert(data.update, 'missing data.update object')
            let ret = await this.repo.upsert(data.query,data.update,data.options)
            return ret;
        }

        if (data.operation==='upsertBatch') {
            assert(data.batch)
            assert(data.batch instanceof Array, 'batch must be an array of { query, update }')

            let res = []
            this.collectPublications = []
            for await (const part of data.batch) {
                try {
                    if (!part.update) continue;
                    let ret = await this.repo.upsert(part.query,part.update)
                    res.push(ret);

                }  catch(err) {
                    console.error('upsertBatch',err)
                }
            };

            this.publishMsg({
                channel: `db/${data.db}/${data.collection}`,
                payload: {
                    db: data.db,
                    operation: 'batch',
                    collection: data.collection,
                    data: this.collectPublications.map(a => a.payload)
                }
            });
            this.collectPublications = null
            
            return res;
        }       

        if (data.operation==='updateOne') {
            assert(data.query, 'missing data.query object')
            assert(data.update, 'missing data.update object')
            let ret = await this.repo.updateOne(data.query,data.update,data.options)
            // if (ret && ret._id) {
            //     this.publishMsgAndAudit(ret._id,'update',removeUnchanged(ret,data.update),data);
            // };
            return ret;
        }   
        if (data.operation==='update') {
            assert(data.query, 'missing data.query object')
            assert(data.update, 'missing data.update object')
            let ret = await this.repo.update(data.query, data.update)
            // if (ret && ret.nModified) {
            //     this.publishMsgAndAudit(ret._id,'updateMany',removeUnchanged(ret,data.update),data);
            // };
            return ret;
        }     
        if (data.operation === 'getCollections') {
            let ret = await this.repo.getCollections();
            return ret;
        }   
        if (data.operation==='find') {
            assert(data.query, 'missing data.query object')
            let ret = await this.repo.find(data.query,data.opts)
            return ret;
        }   
        if (data.operation==='findAll') {
            assert(data.query, 'missing data.query object')
            let ret = await this.repo.findAll(data.query, data.opts)
            return ret;
        }     
        if (data.operation==='deleteOne') {
            assert(data.query, 'missing data.query object')
            let ret = await this.repo.deleteOne(data.query)
            // if (ret && ret._id) {
            //     this.publishMsgAndAudit(ret._id,'delete',ret,data);
            // };            
            return ret;
        }     
        if (data.operation==='delete') {
            assert(data.query, 'missing data.query object')
            let ret = await this.repo.delete(data.query)
            // if (ret && ret.n) {
            //     this.publishMsgAndAudit(ret._id,'deleteMany',ret,data);
            // };            
            return ret;
        }  
        if (data.operation==='hardDeleteOne') {
            assert(data.query, 'missing data.query object')
            let ret = await this.repo.hardDeleteOne(data.query)
            // if (ret && ret._id) {
            //     this.publishMsgAndAudit(ret._id,'delete',ret,data);
            // };            
            return ret;
        }             
        if (data.operation==='hardDelete') {
            assert(data.query, 'missing data.query object')
            let ret = await this.repo.hardDelete(data.query)
            // if (ret && ret.n) {
            //     this.publishMsgAndAudit(ret._id,'deleteMany',ret,data);
            // };            
            return ret;
        }       
        if (data.operation==='blockOne') {
            assert(data.query, 'missing data.query object')
            let ret = await this.repo.blockOne(data.query)
            // if (ret && ret._id) {
            //     this.publishMsgAndAudit(ret._id,'block',ret,data);
            // };            
            return ret;
        }  
        if (data.operation==='unblockOne') {
            assert(data.query, 'missing data.query object')
            let ret = await this.repo.unblockOne(data.query)
            // if (ret && ret._id) {
            //     this.publishMsgAndAudit(ret._id,'unblock',ret,data);
            // };            
            return ret;
        }    
        if (data.operation==='purgeChangesForCollection') {
            assert(data.collection)
            let mongo = new Mongo(data.db)
            // @ts-ignore
            let ret= await mongo.dbLogPurge(data.collection)
            mongo.close()
            return ret
        }   
        if (data.operation==='purgeChangesForEntity') {
            assert(data.collection)
            assert(data.db)
            assert(data._id)
            let mongo = new Mongo(data.db)
            let ret= await mongo.dbLogPurge(data.collection,data._id)
            mongo.close()
            return ret
        }                            
        if (data.operation==='getChangesForCollection') {
            assert(data.collection)
            assert(data.db)
            let mongo = new Mongo(data.db)
            let ret= await mongo.dbLogGet(data.collection,data._id)
            mongo.close()
            return ret
            //   last audit record for this collection
            // auditDb = auditDb || new Mongo();
            // auditDb.setDb(data.db);                        
            // let ret = await auditDb.aggregatethis.auditTableName,[
            //     { $match: { collection: data.collection }},
            //     { $sort: { rev:-1 } },
            // ]);
            // return ret;
        }  
        if (data.operation==='getChangesForEntity') {
            assert(data.collection)
            assert(data.db)
            assert(data.id || data._id)
            let mongo = new Mongo(data.db)
            let ret= await mongo.dbLogGet(data.collection,data.id || data._id)
            mongo.close()
            return ret
            //// assert(data.id,"missing data.id for entity")
            // // find last audit record for this collection
            // auditDb = auditDb || new Mongo();
            // auditDb.setDb(data.db);            
            // let ret = await auditDb.aggregatethis.auditTableName,[
            //     { $match: { entityid: auditDb.objectid(data.id.toString()) }},
            //     { $sort: { rev:-1 } },
            // ]);
            // return ret;
        }   
        if (data.operation==='aggregate') {
            assert(data.pipeline,"missing data.pipeline")
            let ret = await this.repo.aggregate(data.pipeline)
            return ret;
        }     
        if (data.operation==='timestamp') {
            let ret = await this.repo.timestamp()
            return ret;
        } 
        if (data.operation === 'testHash') {
            let ret = await this.repo.testHash(data.query,data.field,data.value||data.unhashed)
            return ret;
        } 
        if (data.operation==='fail') {
            await this.repo.find('a',{},{ project: { a:0, b:1 }})
        }     
        assert(false, 'unsupported operation ' + data.operation)
    }

    // support functions


    removeUnchanged(wholerecord,diff)
    {
        let a = {}
        Object.keys(diff).forEach( k => a[k]=wholerecord[k] )
        if (wholerecord._rev) a._rev = wholerecord._rev;
        if (wholerecord._ts) a._ts = wholerecord._ts;
        a._id = wholerecord._id
        return a;
    }

}
