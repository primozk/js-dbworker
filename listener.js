var ebus = require('cry-ebus');

var client = new ebus.Client();

client.on('message', (msg,channel) => console.log('message on channel',channel, msg));
client.on('error', (msg, more) => console.log('error ', msg, more));

client.subscribe('')

client.start();


process.on('SIGTERM', function () {
    console.log('sigterm')
    client.stop()
    process.exit(0);
});

process.on('SIGINT', function () {
    console.log('SIGINT')
    client.stop()
    process.exit(0);
});

